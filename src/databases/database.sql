CREATE DATABASE rest_api_node;
CREATE TABLE users(
    id SERIAL,
    name VARCHAR(40),
    email Text,
    PRIMARY KEY(id)
);