const { send } = require('express/lib/response');
const { Pool } = require('pg');

const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: 'Admin@2025',
    database: 'rest_api_node'
});

const getUsers = async (req, res)=>{
    const response = await pool.query('SELECT * FROM users');
    res.status(200).json(response.rows);
}

const updateUser = async (req, res)=>{
    const id = parseInt(req.params.id);
    const { name, email } = req.body;
    const response = await pool.query('UPDATE users SET name=$1, email=$2 WHERE id = $3', [
        name,
        email,
        id]);
    res.status(205).json('user updated');
}

const deleteUser = async (req, res)=>{
    const id = parseInt(req.params.id);
    const response = await pool.query('DELETE FROM users WHERE id = $1', [id]);
    res.status(200).json('User deleted');
}

const getUser = async (req, res)=>{
    const id  = req.params.id;
    const response = await pool.query("SELECT name, email FROM users WHERE id = $1", [id]);
    res.status(200).json({
        message: 'user found',
        'data': response.rows 
    });
}

const createUser = async (req, res)=>{
    //const response = await pool.query('re')
    const { name, email } = req.body;
    const response = await pool.query('INSERT INTO users(name, email) VALUES ($1,$2)', [name, email]);
    res.json({
        message: 'User added succesfully',
        'data': {name, email}
    });
}

module.exports = {
    getUsers,
    getUser,
    deleteUser,
    updateUser,
    createUser
}