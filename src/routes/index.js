const { Router } = require('express');
const router = Router();

const { getUsers, getUser, deleteUser, createUser, updateUser } = require('../controllers/index.controller');

router.get('/users', getUsers);
router.get('/user/:id', getUser);
router.delete('/user/:id', deleteUser);
router.put('/user/:id', updateUser);
router.post('/create-user', createUser);

module.exports = router;